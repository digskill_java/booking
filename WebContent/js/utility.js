/**
 * イベントリスナーに登録を行います。
 *
 * @param {object}
 *            obj イベントを登録する対象オブジェクト
 * @param {string}
 *            type イベント名称
 * @param {function}
 *            func イベント発火時に呼び出す関数
 */
function addEvent(obj, type, func) {
	if (obj.addEventListener) {
		obj.addEventListener(type, func, false);
	} else {
		if (obj.attachEvent) {
			obj.attachEvent('on' + type, func);
		}
	}
}

/**
 * イベントリスナーの削除を行います。
 *
 * @param {object}
 *            obj イベントを削除する対象オブジェクト
 * @param {string}
 *            type イベント名称
 * @param {function}
 *            func イベント発火時に呼び出す関数
 */
function removeEvent(obj, type, func) {
	if (obj.removeEventListener) {
		obj.removeEventListener(type, func, false);
	} else {
		if (obj.detachEvent) {
			obj.detachEvent('on' + type, func);
		}
	}
}

function getBaseQuery(action) {
	return env.index_url + '?a=' + action;
}

String.prototype.left = function(n) {
	var str = this.valueOf();
	str = str.substr(0, n);
	return str;
};

String.prototype.right = function(n) {
	var str = this.valueOf();
	str = str.substr(str.length - n, n);
	return str;
};

var XHRController = function() {
	this.initialize.apply(this, arguments);
};
XHRController.prototype = {
	xhr : undefined,
	initialize : function() {
		this.xhr = new XMLHttpRequest();
	},
	send : function(url, method, data, async, callback, contenttype, noeval) {
		var self = this;
		this.xhr.onreadystatechange = function() {
			if (self.xhr.readyState === 4 && self.xhr.status === 200) {
				if (noeval) {
					callback(self.xhr.responseText);
				} else {
					try {
						callback(eval('(' + self.xhr.responseText + ')'));
					} catch (e) {
						if (DEBUG) {
							new ErrorWindow(self.xhr.responseText);
							console.log(e);
						} else {
							alert('処理に失敗してしまいました！\nトップページに戻ります。');
							locationHref(env.index_url);
						}
					}
				}
			}
		};
		this.xhr.open(method, url, async);
		this.xhr.setRequestHeader("Content-Type", contenttype);
		this.xhr.send(data);
	}
};

var ErrorWindow = function() {
	this.initialize.apply(this, arguments);
};
ErrorWindow.prototype = {
	window : undefined,
	initialize : function(innerHTML) {
		var new_window = window.open('', 'error', "width=800,height=600");
		new_window.document.open();
		new_window.document
				.write('<html><head><title>error! [debug mode]</title><body>');
		new_window.document.writeln(innerHTML);
		new_window.document.write('</body></html>');
		new_window.document.close();
	}
};

var Draggable = function() {
	this.initialize.apply(this, arguments);
};
Draggable.prototype = {
	elem : undefined,
	listener : undefined,
	dragging : undefined,
	offsetx : undefined,
	offsety : undefined,
	x : undefined,
	y : undefined,
	initialize : function(elem, listener) {
		var self = this;
		self.elem = elem;
		self.listener = listener;
		self.listener.onselectstart = function() {
			return false;
		};
		self.listener.onmousedown = function(e) {
			self.onmousedown.call(self, e);
		};
		self.listener.onmouseup = function(e) {
			self.onmouseup.call(self, e);
		};

		self.dragging = false;
		self.offsetx = 0;
		self.offsety = 0;
		self.x = 0;
		self.y = 0;
	},
	onmousedown : function(e) {
		var self = this;
		var evtobj = window.event ? window.event : e;
		self.offsetx = parseInt(self.elem.offsetLeft);
		self.offsety = parseInt(self.elem.offsetTop);
		self.x = evtobj.clientX;
		self.y = evtobj.clientY;
		self.dragging = true;
		addEvent(self.listener, 'mousemove', function(e) {
			self.onmousemove.call(self, e);
		});
		addEvent(self.listener, 'mouseout', function(e) {
			self.onmouseup.call(self, e);
		});
	},
	onmouseup : function(e) {
		var self = this;
		self.dragging = false;
		removeEvent(self.listener, 'mousemove', function(e) {
			self.onmousemove.call(self, e);
		});
	},
	onmousemove : function(e) {
		var self = this;
		if (self.dragging) {
			var evtobj = window.event ? window.event : e;
			self.elem.style.left = self.offsetx + evtobj.clientX - self.x
					+ "px";
			// self.elem.style.top = self.offsety + evtobj.clientY - self.y +
			// "px";
			return false;
		}
	}
};
