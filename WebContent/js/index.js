var calendar_controller = {
	target_change : function(obj) {
		var self = this;
		if (!obj.disabled) {
			obj.disabled = true;
			now_loading.on();
			self.target_select_form_submit();
		}
	},
	month_move : function(obj, target_month) {
		var self = this;
		if (!obj.disabled) {
			obj.disabled = true;
			now_loading.on();
			target_select_form.tm.value = target_month;
			self.target_select_form_submit();
		}
	},
	target_select_form_submit : function() {
		var self = this;
		var url = getBaseQuery('index');
		location.href = self.get_calender_controll_query(url);
	},
	get_calender_controll_query : function(url) {
		url += '&tc=' + target_select_form.tc.value;
		url += '&tm=' + target_select_form.tm.value;
		return url;
	}
};

var input_dialog_controller = {
	dialog : undefined,
	continuous : false,
	reload_when_close : false,
	setup : function() {
		var self = this;
		if (self.dialog == undefined) {
			self.dialog = document.getElementById('input_dialog_div');
		}
		var draggable = new Draggable(self.dialog, document
				.getElementById('input_dialog_title_div'));
		self.dialog.style.display = 'none';
		addEvent(window, 'resize', function() {
			mask.set_size();
			input_dialog_controller.set_size();
		});
	},
	modify : function(id) {
		now_loading.on();
		var self = this;
		self.get_booking_data(id);
	},
	get_booking_data : function(id) {
		var self = this;
		var url = getBaseQuery('get');
		var data = '&id=' + id;
		var xhr_controller = new XHRController();
		xhr_controller.send(url, 'POST', data, true,
				self.get_booking_data_result, env.content_type);
	},
	get_booking_data_result : function(response) {
		if (response.succeeded) {
			input_dialog_controller.input_modify(response.data);
		} else {
			alert(response.message);
			calendar_controller.target_select_form_submit();
		}
	},
	input_modify : function(data) {
		now_loading.on();
		var self = this;
		booking_form.booking_id.value = data['id'];
		booking_form.booking_update_datetime.value = data['updateDatetime'];
		booking_form.booking_date.value = data['startDatetime'].left(10);
		booking_form.booking_start_time.value = data['startDatetime'].right(5);
		booking_form.booking_end_time.value = data['endDatetime'].right(5);
		booking_form.booking_title.value = data['title'];
		booking_form.booking_user_name.value = data['userName'];
		// booking_form.booking_user_section_name.value = data['userSectionName'];
		// booking_form.booking_user_phone_number.value = data['userPhoneNumber'];
		// booking_form.booking_member_count.value = data['memberCount'];
		booking_form.booking_description.value = data['description'];
		document.getElementById('booking_mode_display').innerText = '予約内容照会　(最終更新日時：'
				+ data['updateDatetime'] + ')';
		document.getElementById('input_dialog_save_button').innerText = '変更';
		// document.getElementById('input_dialog_cancel_button').style.display = 'inline-block';
		self.show();
		now_loading.off();
	},
	input_new : function(start_datetime) {
		now_loading.on();
		var self = this;
		booking_form.booking_id.value = '';
		booking_form.booking_update_datetime.value = '';
		booking_form.booking_date.value = start_datetime.left(10);
		booking_form.booking_start_time.value = start_datetime.right(5);
		booking_form.booking_end_time.selectedIndex = booking_form.booking_start_time.selectedIndex;
		booking_form.booking_title.value = '';
		booking_form.booking_user_name.value = '';
		// booking_form.booking_user_section_name.value = '';
		// booking_form.booking_user_phone_number.value = '';
		// booking_form.booking_member_count.selectedIndex = 0;
		booking_form.booking_description.value = '';
		document.getElementById('booking_mode_display').innerText = '新規予約';
		document.getElementById('input_dialog_save_button').innerText = '予約';
		// document.getElementById('input_dialog_cancel_button').style.display = 'none';
		self.show();
		now_loading.off();
	},
	copy : function() {
		now_loading.on();
		var self = this;
		booking_form.booking_id.value = '';
		booking_form.booking_update_datetime.value = '';
		document.getElementById('booking_mode_display').innerText = '新規予約';
		document.getElementById('input_dialog_save_button').innerText = '予約';
		// document.getElementById('input_dialog_cancel_button').style.display = 'none';
		self.show();
		now_loading.off();
	},
	show : function() {
		mask.on();
		var self = this;
		self.dialog.style.display = 'block';
		self.set_size();
	},
	set_size : function() {
		var self = this;
		var top = 10;
		var left = (document.documentElement.clientWidth / 2);
		left -= (self.dialog.clientWidth / 2);
		self.dialog.style.top = top + 'px';
		self.dialog.style.left = left + 'px';
	},
	close : function() {
		var self = this;
		if (self.reload_when_close) {
			calendar_controller.target_select_form_submit();
		} else {
			self.dialog.style.display = 'none';
			mask.off();
		}
	},
	get_dialog_data : function() {
		var data = '';
		data += '&id=' + (booking_form.booking_id.value - 0);
		data += '&update_datetime='
				+ encodeURIComponent(booking_form.booking_update_datetime.value);
		data += '&start_datetime='
				+ encodeURIComponent(booking_form.booking_date.value + ' '
						+ booking_form.booking_start_time.value);
		data += '&end_datetime='
				+ encodeURIComponent(booking_form.booking_date.value + ' '
						+ booking_form.booking_end_time.value);
		data += '&title='
				+ encodeURIComponent(booking_form.booking_title.value);
		data += '&user_name='
				+ encodeURIComponent(booking_form.booking_user_name.value);
		// data += '&user_section_name='
		// 		+ encodeURIComponent(booking_form.booking_user_section_name.value);
		// data += '&user_phone_number='
		// 		+ encodeURIComponent(booking_form.booking_user_phone_number.value);
		// data += '&member_count=' + (booking_form.booking_member_count.value - 0);
		data += '&description='
				+ encodeURIComponent(booking_form.booking_description.value);
		return data;
	},
	validation : function() {
		var messages = '';
		if (booking_form.booking_start_time.value >= booking_form.booking_end_time.value) {
			messages += '【利用日時】を正しく入力してください。\n';
		}
		if (booking_form.booking_title.value == '') {
			messages += '【タイトル】を入力してください。\n';
		}
		if (booking_form.booking_user_name.value == '') {
			messages += '【予約者名】を入力してください。\n';
		}
		if (messages != '') {
			alert(messages);
			return false;
		}
		return true;
	},
	save_continuous : function() {
		var self = this;
		self.continuous = true;
		self.reload_when_close = true;
		self.save();
	},
	save : function() {
		var self = this;
		var url = getBaseQuery('input');
		url = calendar_controller.get_calender_controll_query(url);
		if (self.validation()) {
			now_loading.on();
			var data = self.get_dialog_data();
			var xhr_controller = new XHRController();
			xhr_controller.send(url, 'POST', data, true, self.save_result,
					env.content_type);
		}
	},
	save_result : function(response) {
		if (response.succeeded) {
			alert(response.message);
			if (!input_dialog_controller.continuous) {
				calendar_controller.target_select_form_submit();
			} else {
				now_loading.off();
			}
		} else {
			alert(response.message);
			now_loading.off();
		}
		input_dialog_controller.continuous = false;
	},
	cancel : function() {
		var self = this;
		var url = getBaseQuery('cancel');
		url = calendar_controller.get_calender_controll_query(url);
		if (confirm('本当にこの予約をキャンセルしますか？')) {
			now_loading.on();
			var data = self.get_dialog_data();
			var xhr_controller = new XHRController();
			xhr_controller.send(url, 'POST', data, true, self.save_result,
					env.content_type);
		}
	},
	cancel_result : function(response) {
		if (response.succeeded) {
			alert(response.message);
			calendar_controller.target_select_form_submit();
		} else {
			alert(response.message);
			now_loading.off();
		}
	}

};

addEvent(window, 'load', function() {
	input_dialog_controller.setup();
});
