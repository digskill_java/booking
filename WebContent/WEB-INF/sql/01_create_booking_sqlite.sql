-- 予約対象マスタ
DROP TABLE IF EXISTS booking_target;
CREATE TABLE IF NOT EXISTS booking_target (
	id integer primary key autoincrement,
	code text,
	name text,
	information text
);

-- 予約データ
DROP TABLE IF EXISTS booking_data;
CREATE TABLE IF NOT EXISTS booking_data (
	id integer primary key autoincrement,
	target_code text,
	start_datetime text,
	end_datetime text,
	user_name text,
	user_section_name text,
	user_phone_number text,
	member_count integer,
	title text,
	description text,
	update_datetime text
);

-- 予約対象マスタのテスト用デフォルトデータ
INSERT INTO booking_target (code, name, information)
VALUES ('room_a', '会議室Ａ', '会議室Ａには１０人座れます。
ホワイトボードが１枚あり、小規模な打ち合わせに利用できます。');
INSERT INTO booking_target (code, name, information)
VALUES ('room_b', '会議室Ｂ', '会議室Ｂには３０人座れます。
ホワイトボードが４枚あり、プロジェクターも備え付けのため、
グループワークのある研修などにも対応可能です。');
INSERT INTO booking_target (code, name, information)
VALUES ('projector_01', 'プロジェクター１号', 'エプソン
高輝度のため電気をつけたままでも問題なく使用できます。');
