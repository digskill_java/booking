<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="jp.co.digskill.booking.model.dto.BookingDataDto"%>

<%
	// カレンダーで使用する日付フォーマッタ
	SimpleDateFormat headerDateFormat = new SimpleDateFormat("yyyy/MM/dd E");
	SimpleDateFormat labelDateFormat = new SimpleDateFormat("yyyy/MM/dd(E)");
	SimpleDateFormat valueDateFormat = new SimpleDateFormat("yyyy/MM/dd");
	SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

	// カレンダー表示範囲制御用変数
	String targetMonth = (String) request.getAttribute("targetMonth");
	int bookingTimeSlotStartHour = (int) request.getAttribute("bookingTimeSlotStartHour");
	int bookingTimeSlotEndHour = (int) request.getAttribute("bookingTimeSlotEndHour");

	// 予約情報リスト
	@SuppressWarnings("unchecked")
	List<BookingDataDto> bookingList = (List<BookingDataDto>) request.getAttribute("bookingList");
%>

<div class="calendar_div">
	<table class="calendar_table">

		<!-- 時間帯表示：先頭（動的インクルード） -->
		<thead><jsp:include page="time_slot_tr.jsp" flush="true" /></thead>

		<!-- カレンダー部本体 -->
		<tbody>
			<%
				// 日付ループ用のカレンダーオブジェクト
				Calendar startDate = (Calendar) request.getAttribute("startDate");
				Calendar endDate = (Calendar) request.getAttribute("endDate");
				Calendar currentDate = (Calendar) startDate.clone();

				// 予約データを先頭から取得するためのインデックス（予約開始日時でソート済み前提）
				int bookingListIndex = 0;

				// 月初～月末の日付ループ
				while (currentDate.before(endDate)) {
					String headerDate = headerDateFormat.format(currentDate.getTime());

					// １０日ごとにヘッダー行をつける（先頭行は除く）
					int day = currentDate.get(Calendar.DATE);
					if (day == 11 || day == 21) {
			%>
			<!-- 時間帯表示：１０日毎（動的インクルード） -->
			<jsp:include page="time_slot_tr.jsp" flush="true" />
			<%
				}
			%>
			<tr>
				<%
					// 今日の強調表示用
						Calendar today = Calendar.getInstance();
						String headerDateThClass = "date_th";
						String plotTdClass = "plot_td";
				%>
				<th class="<%=headerDateThClass%>"><%=headerDate%></th>
				<%
					// 15分刻みの時刻ループ処理用のカレンダー
						Calendar currentTime = (Calendar) currentDate.clone();
						currentTime.set(Calendar.HOUR_OF_DAY, bookingTimeSlotStartHour);
						currentTime.set(Calendar.MINUTE, 0);

						// 15分刻みの時刻ループ処理終了用のカレンダー
						Calendar endTime = (Calendar) currentTime.clone();
						endTime.set(Calendar.HOUR_OF_DAY, bookingTimeSlotEndHour);

						// currentTimeの枠の終了時刻（15分後）用のカレンダー
						Calendar currentTimeEnd = (Calendar) currentTime.clone();
						currentTimeEnd.add(Calendar.MINUTE, 15);

						// カレンダーに埋め込む予約データ表示関連の準備
						int cols = 0;
						String cssClass = "";
						String onClickEvent = "";
						String title = "";
						String colspan = "";
						boolean display = false;

						while (currentTime.before(endTime)) {

							colspan = "";
							display = false;

							if (bookingListIndex < bookingList.size()) {
								BookingDataDto bookingData = bookingList.get(bookingListIndex);
								String startDateString = bookingData.getStartDatetime();
								String endDateString = bookingData.getEndDatetime();

								// 時間帯の開始時刻が予約開始時刻ならばとりあえず結合開始（colspan=1）
								if (dbDateFormat.format(currentTime.getTime()).equals(startDateString)) {
									cols = 1;
								}

								// 時間帯の終了時刻が予約終了時刻でない場合は次の時間帯と結合確定（colspanを増やす）
								if (cols > 0 && !dbDateFormat.format(currentTimeEnd.getTime()).equals(endDateString)) {
									cols++;
								}

								// 時間帯の終了時刻が予約終了時刻ならば
								if (dbDateFormat.format(currentTimeEnd.getTime()).equals(endDateString)) {

									// <td>タグの属性を設定
									colspan = "colspan=" + cols;
									cssClass = plotTdClass + " plotted border_left";
									onClickEvent = "input_dialog_controller.modify('" + bookingData.getId() + "');";
									title = new StringBuilder(bookingData.getTitle())
											.append("\r\n")
											.append(bookingData.getStartDatetime())
											.append(" ～ ")
											.append(bookingData.getEndDatetime().substring(11))
											.append("\r\n")
											.append(bookingData.getUserName())
											.append("\r\n")
											.append("最終更新日時：")
											.append(bookingData.getUpdateDatetime())
											.toString();

									// <td>タグ出力準備ＯＫ
									display = true;

									// 予約データも次に備える
									bookingListIndex++;
								}
							}

							// 予約なしの場合
							if (cols == 0) {

								// <td>タグの属性を設定
								String startDateString = dbDateFormat.format(currentTime.getTime());
								onClickEvent = new StringBuilder("input_dialog_controller.input_new('").append(startDateString)
										.append("');").toString();

								// 時間帯の切れ目ならば左罫線設定
								cssClass = (currentTime.get(Calendar.MINUTE) == 0) ? plotTdClass + " no_plotted border_left"
										: plotTdClass + " no_plotted";
								title = new StringBuilder("★クリックで予約開始！")
										.append("\r\n")
										.append(labelDateFormat.format(currentTime.getTime()))
										.append(" ")
										.append(timeFormat.format(currentTime.getTime()))
										.append("～")
										.toString();

								// <td>タグ出力準備ＯＫ
								display = true;
							}

							// 予約なしまたは予約終了の場合、<td>を出力する
							if (display) {
				%>
				<td class="<%=cssClass%>" <%=colspan%> onclick="<%=onClickEvent%>"
					title="<%=title%>"></td>
				<%
					cols = 0;
								cssClass = "";
								onClickEvent = "";
								title = "";
							}

							// 時間帯ループ用時刻をそれぞれ＋１５分
							currentTime.add(Calendar.MINUTE, 15);
							currentTimeEnd.add(Calendar.MINUTE, 15);
						}
				%>
			</tr>
			<%
				// 日付ループ用日付を＋１日
					currentDate.add(Calendar.DATE, 1);
				}
			%>
		</tbody>
	</table>
</div>