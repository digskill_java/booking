<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%
	// 時間帯表示に使用する日付フォーマッタ
	SimpleDateFormat timeSlotFormat = new SimpleDateFormat("HH");

	// 時間帯表示範囲制御用変数
	int bookingTimeSlotStartHour = (int) request.getAttribute("bookingTimeSlotStartHour");
	int bookingTimeSlotEndHour = (int) request.getAttribute("bookingTimeSlotEndHour");

	// 時間帯表示処理用のカレンダー
	Calendar currentHour = Calendar.getInstance();
	currentHour.set(Calendar.HOUR_OF_DAY, bookingTimeSlotStartHour);

	// 時間帯表示処理終了用のカレンダー
	Calendar endHour = (Calendar) currentHour.clone();
	endHour.set(Calendar.HOUR_OF_DAY, bookingTimeSlotEndHour);
%>
<tr>
	<th class="date_head_th"></th>
	<%
		// 時間帯表示
		while (currentHour.before(endHour)) {
			String timeSlot = timeSlotFormat.format(currentHour.getTime());
	%>
	<th class="time_slot_th" colspan="4"><%=timeSlot%>時</th>
	<%
		currentHour.add(Calendar.HOUR_OF_DAY, 1);
		}
	%>
</tr>
