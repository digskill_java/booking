<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="jp.co.digskill.booking.model.dto.BookingTargetDto"%>

<%
	// 入力ダイアログで使用する日付フォーマッタ
	SimpleDateFormat labelDateFormat = new SimpleDateFormat("yyyy/MM/dd(E)");
	SimpleDateFormat valueDateFormat = new SimpleDateFormat("yyyy/MM/dd");
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

	// 時間帯プルダウン表示範囲制御用変数
	int bookingTimeSlotStartHour = (int) request.getAttribute("bookingTimeSlotStartHour");
	int bookingTimeSlotEndHour = (int) request.getAttribute("bookingTimeSlotEndHour");

	// 現在の予約対象オブジェクト
	BookingTargetDto bookingTarget = (BookingTargetDto) request.getAttribute("bookingTarget");

	// 予約対象リスト
	@SuppressWarnings("unchecked")
	List<BookingTargetDto> targetList = (List<BookingTargetDto>) request.getAttribute("targetList");
%>

<div id="input_dialog_div">
	<form id="booking_form" onsubmit="return false;">
		<input type="text" id="booking_id" id="booking_id"
			style="display: none;" value="" /> <input type="text"
			id="booking_update_datetime" style="display: none;" value="" />
		<div id="input_dialog_title_div"
			title="<%=bookingTarget.getInformation()%>">
			<span id="target_name_display"><%=bookingTarget.getName()%></span><br />
			<span id="booking_mode_display"></span>
		</div>
		<div id="booking_datetime_div" class="booking_require_items"
			title="【必須入力項目】
予約日時を入力して下さい。">
			利用日時： <select id="booking_date" name="booking_date">
				<%
					// 半年先まで予約可能
					Calendar startDate = (Calendar) request.getAttribute("startDate");
					Calendar endDate = (Calendar) request.getAttribute("endDate");
					endDate.add(Calendar.MONTH, 5);
					Calendar currentDate = (Calendar) startDate.clone();

					// 利用日時プルダウン生成
					while (!currentDate.after(endDate)) {
						String optionValue = valueDateFormat.format(currentDate.getTime());
						String optionLabel = labelDateFormat.format(currentDate.getTime());
				%>
				<option value="<%=optionValue%>"><%=optionLabel%></option>
				<%
					currentDate.add(Calendar.DATE, 1);
					}
				%>
			</select> <select id="booking_start_time" name="booking_start_time">
				<%
					// 15分刻みのプルダウン生成処理用のカレンダー
					Calendar currentTime = Calendar.getInstance();
					currentTime.set(Calendar.HOUR_OF_DAY, bookingTimeSlotStartHour);
					currentTime.set(Calendar.MINUTE, 0);

					// 15分刻みのプルダウン生成処理終了用のカレンダー
					Calendar endTime = (Calendar) currentTime.clone();
					endTime.set(Calendar.HOUR_OF_DAY, bookingTimeSlotEndHour);

					// 15分刻みのプルダウン生成
					// 開始時刻から終了時刻の15分前まで！
					while (currentTime.before(endTime)) {
						String time = timeFormat.format(currentTime.getTime());
				%>
				<option value="<%=time%>"><%=time%></option>
				<%
					currentTime.add(Calendar.MINUTE, 15);
					}
				%>
			</select> ～ <select id="booking_end_time" name="booking_end_time">
				<%
					// 15分刻みのプルダウン生成処理用のカレンダー
					currentTime.set(Calendar.HOUR_OF_DAY, bookingTimeSlotStartHour);
					currentTime.set(Calendar.MINUTE, 0);

					// 15分刻みのプルダウン生成
					// 開始時刻の15分後から終了時刻まで！
					currentTime.add(Calendar.MINUTE, 15);
					while (!currentTime.after(endTime)) {
						String time = timeFormat.format(currentTime.getTime());
				%>
				<option value="<%=time%>"><%=time%></option>
				<%
					currentTime.add(Calendar.MINUTE, 15);
					}
				%>
			</select>
		</div>

		<div id="booking_title_div" class="booking_require_items"
			title="【必須入力項目】
利用目的や会議体名など、
わかりやすい名称を入力して下さい。">
			タイトル：<input type="text" id="booking_title" name="booking_title"
				value="" />
		</div>

		<div id="booking_user_name_div" class="booking_require_items"
			title="【必須入力項目】
予約される方や代表者の方、
もしくは問い合わせ先の方の
お名前を入力して下さい。">
			予約者名：<input type="text" id="booking_user_name"
				name="booking_user_name" value="" />
		</div>

		<div id="booking_description_div" class="booking_items"
			title="【任意入力項目】
利用目的の詳細や、
参加者への通知事項など
自由に入力して下さい。">
			利用詳細・その他備考：<br />
			<textarea id="booking_description" name="booking_description"></textarea>
		</div>

		<div class="input_dialog_button" id="input_dialog_close_button"
			onclick="input_dialog_controller.close();"
			title="保存せずに画面を閉じます。
※編集中の内容は保存されません！">閉じる</div>

		<div class="input_dialog_button" id="input_dialog_save_button"
			onclick="input_dialog_controller.save();"
			title="編集中の内容を正式予約として保存します。">ボタン名</div>

		<div class="input_dialog_button" id="input_dialog_cancel_button"
			onclick="input_dialog_controller.cancel();" style="display: none;"
			title="表示中の予約を取り消します。">予約取消</div>

	</form>
</div>
