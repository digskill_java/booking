<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="${pageContext.request.contextPath}/css/reset.css"
	media="screen,print" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/index.css"
	media="screen,print" rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/utility.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/index.js"></script>
<title>予約システム</title>
<style type="text/css">
<!--
div#now_loading {
	position: fixed;
	top: 0px;
	left: 0px;
	z-index: 99;
	background: white;
	-moz-opacity: 0.4;
	opacity: 0.40;
	filter: Alpha(opacity = 40);
}

td#now_loading_td {
	text-align: center;
}

div#now_loading_message {
	background-image:
		url(${pageContext.request.contextPath}/css/images/loading.gif);
	background-repeat: no-repeat;
	background-position: center;
	font-size: 16px;
	font-weight: bold;
}

div#mask {
	position: fixed;
	top: 0px;
	left: 0px;
	z-index: 10;
	background: white;
	-moz-opacity: 0.4;
	opacity: 0.40;
	filter: Alpha(opacity = 40);
}
-->
</style>