<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.Calendar"%>
<%@ page import="java.text.SimpleDateFormat"%>

<%
	// 表示年月選択エリアで使用する日付フォーマッタ
	SimpleDateFormat yyyymm = new SimpleDateFormat("yyyyMM");
	SimpleDateFormat yyyymmJp = new SimpleDateFormat("yyyy年MM月");

	// 表示年月
	String targetMonthYyyyMm = (String) request.getAttribute("targetMonth");
	int bookingRangeOffsetYears = (int) request.getAttribute("bookingRangeOffsetYears");

	// 表示年月のカレンダー
	Calendar targetMonth = Calendar.getInstance();
	targetMonth.set(Calendar.YEAR, Integer.parseInt(targetMonthYyyyMm.substring(0, 4)));
	targetMonth.set(Calendar.MONTH, Integer.parseInt(targetMonthYyyyMm.substring(4, 6)) - 1);
	targetMonth.set(Calendar.DATE, 1);

	// 表示年月プルダウン表示処理用のカレンダー
	Calendar currentMonth = (Calendar) targetMonth.clone();
	currentMonth.add(Calendar.YEAR, bookingRangeOffsetYears * -1);

	// 表示年月プルダウン表示処理終了用のカレンダー
	Calendar endMonth = (Calendar) targetMonth.clone();
	endMonth.add(Calendar.YEAR, bookingRangeOffsetYears);

	String monthValue = "";
	String monthTextJp = "";
%>

<div class="month_selector_div">
	表示年月選択： <select class="month_select_select"
		onchange="calendar_controller.month_move(this,this.value);"
		title="表示する年月を選択して下さい。">
		<%
			// 表示年月選択プルダウン生成
			String selected = "";
			while (currentMonth.before(endMonth)) {
				monthValue = yyyymm.format(currentMonth.getTime());
				monthTextJp = yyyymmJp.format(currentMonth.getTime());
				selected = "";
				if (monthValue.equals(targetMonthYyyyMm)) {
					selected = "selected=\"selected\"";
				}
		%>
		<option value="<%=monthValue%>" <%=selected%>><%=monthTextJp%></option>
		<%
			currentMonth.add(Calendar.MONTH, 1);
			}
		%>
	</select>

	<%
		// 表示年月の１月前
		currentMonth = (Calendar) targetMonth.clone();
		currentMonth.add(Calendar.MONTH, -1);
		monthValue = yyyymm.format(currentMonth.getTime());
		monthTextJp = yyyymmJp.format(currentMonth.getTime());
	%>
	<span class="month_select_link"
		onclick="calendar_controller.month_move(this,'<%=monthValue%>');"
		title="表示中年月の前月に移動します。">&lt;&lt;&nbsp;前月（<%=monthTextJp%>）
	</span>

	<%
		// 今月
		currentMonth = Calendar.getInstance();
		monthValue = yyyymm.format(currentMonth.getTime());
		monthTextJp = yyyymmJp.format(currentMonth.getTime());
	%>
	<span class="month_select_link"
		onclick="calendar_controller.month_move(this,'<%=monthValue%>');"
		title="今月に移動します。">今月（<%=monthTextJp%>）
	</span>

</div>
