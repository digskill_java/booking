<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="now_loading" style="display: none;">
	<table>
		<tr>
			<td id="now_loading_td">
				<div id="now_loading_message">
					<br /> <br /> <br /> <br /> データ送受信中です。しばらくお待ちください。
				</div>
			</td>
		</tr>
	</table>
</div>

<div id="mask" style="display: none;"></div>

<script type="text/javascript">
	//<![CDATA[
	var DEBUG = true;
	var env = {
		index_url : '${pageContext.request.contextPath}/index/',
		content_type : 'application/x-www-form-urlencoded;charset=UTF-8'
	};
	var now_loading = {
		layer : undefined,
		td : undefined,
		on : function() {
			if (!now_loading.layer) {
				now_loading.td = document.getElementById('now_loading_td');
				now_loading.layer = document.getElementById('now_loading');
			}
			now_loading.td.style.height = document.documentElement.clientHeight
					+ 'px';
			now_loading.td.style.width = document.documentElement.clientWidth
					+ 'px';
			now_loading.layer.style.display = 'block';
		},
		off : function() {
			now_loading.layer.style.display = 'none';
		}
	};
	now_loading.on();
	var mask = {
		layer : undefined,
		on : function() {
			if (!mask.layer) {
				mask.layer = document.getElementById('mask');
			}
			mask.set_size();
			mask.layer.style.display = 'block';
		},
		set_size : function() {
			if (!mask.layer) {
				mask.layer = document.getElementById('mask');
			}
			mask.layer.style.height = document.documentElement.clientHeight
					+ 'px';
			mask.layer.style.width = document.documentElement.clientWidth
					+ 'px';
		},
		off : function() {
			mask.layer.style.display = 'none';
		}
	};
	//]]>
</script>
