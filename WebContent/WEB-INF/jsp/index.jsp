<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja">
<head>

<!-- head の中身（静的インクルード） -->
<%@ include file="head.jsp"%>

</head>
<body>

	<!-- now_loading および mask 表示関連（静的インクルード） -->
	<%@ include file="now_loading.jsp"%>

	<div id="container" style="width:${containerWidth}px;">

		<!-- 予約対象選択プルダウン（動的インクルード） -->
		<jsp:include page="target_selector_div.jsp" />

		<!-- 表示年月選択プルダウン：上部（動的インクルード） -->
		<jsp:include page="month_selector_div.jsp" />

		<!-- カレンダー部分（動的インクルード） -->
		<jsp:include page="calendar_div.jsp" />

		<!-- 予約内容参照・編集画面（動的インクルード） -->
		<jsp:include page="input_dialog_div.jsp" />

	</div>
	<script type="text/javascript">
		//<![CDATA[
		now_loading.off();
		//]]>
	</script>
</body>
</html>
