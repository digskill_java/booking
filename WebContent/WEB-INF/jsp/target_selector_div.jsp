<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="jp.co.digskill.booking.model.dto.BookingTargetDto"%>

<%
	String targetMonth = (String) request.getAttribute("targetMonth");
	BookingTargetDto bookingTarget = (BookingTargetDto) request.getAttribute("bookingTarget");
	@SuppressWarnings("unchecked")
	List<BookingTargetDto> targetList = (List<BookingTargetDto>) request.getAttribute("targetList");
	String selected = "selected=\"selected\"";
%>
<div class="target_selector_div"
	title="【予約対象情報】
<%=bookingTarget.getInformation()%>">
	<form id="target_select_form" method="post">
		予約対象選択： <select id="tc" name="tc"
			onchange="calendar_controller.target_change(this);">
			<option value="<%=targetList.get(0).getCode()%>" <%=selected%>><%=targetList.get(0).getName()%></option>
		</select> <input type="text" id="tm" name="tm" style="display: none;"
			value="<%=targetMonth%>" />
	</form>
</div>
