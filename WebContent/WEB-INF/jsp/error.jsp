<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.Date"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="ja" xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=11" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="${pageContext.request.contextPath}/css/reset.css"
	media="screen,print" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/common.css"
	media="screen,print" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/error.css"
	media="screen,print" rel="stylesheet" type="text/css" />
<title>予約システム</title>
</head>
<body>
	<div id="container" style="width:${containerWidth}px;">
		<div>
			エラーが発生しました。<br />お手数ですが <a
				href="${pageContext.request.contextPath}/index/">トップページ</a>
			から再度手続きを行ってください。
			<hr />
			[エラー内容]
			<%=new Date()%><br /> ${errorMessage}
		</div>
	</div>
</body>
</html>