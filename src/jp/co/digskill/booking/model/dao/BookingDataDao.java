package jp.co.digskill.booking.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import jp.co.digskill.booking.exception.DataAccessException;
import jp.co.digskill.booking.model.dto.BookingDataDto;

/**
 * 予約情報データテーブル操作クラスです。<br>
 * 更新系操作時に発生したフィードバックは<br>
 * {@code getLatestMessage}によって提供します。<br>
 */
public class BookingDataDao extends AbstractDao {

	/**
	 * 更新系操作時のフィードバックを保持します。<br>
	 */
	private String latestMessage = "";

	/**
	 * 更新系操作時のフィードバックを返却します。<br>
	 *
	 * @return String
	 */
	public String getLatestMessage() {
		return latestMessage;
	}

	/**
	 * 更新系操作時のフィードバックを上書きします。<br>
	 *
	 * @param latestMessage String
	 */
	public void setLatestMessage(String latestMessage) {
		this.latestMessage = latestMessage;
	}

	/**
	 * コンストラクタ<br>
	 * データソースとそのRDBMSの種類を受け取り、<br>
	 * スーパークラスのコンストラクタを呼びます。<br>
	 *
	 * @param ds {@link DataSource} context.xmlから生成したデータソース
	 * @param dbType String web.xmlから取得したRDBMSの種類
	 */
	public BookingDataDao(DataSource ds, String dbType) {
		super(ds, dbType);
	}

	/**
	 * IDに対応する予約情報を１件取得・返却します。<br>
	 * その際、論理削除済みデータは除外します。<br>
	 *
	 * @param id int 取得対象予約の予約ID
	 * @return {@link BookingDataDto} 予約情報クラスのインスタンス
	 * @throws DataAccessException "Data select failed."
	 */
	public BookingDataDto selectData(int id) throws DataAccessException {
		BookingDataDto bookingData = null;
		Connection con = this.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM booking_data WHERE id = ?;";

		try {
			// 用意したSQLにIDを埋め込み（バインド）、実行
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();

			// レコードセットの１行目に移動し、DTOに値を詰める
			if (rs.next()) {
				bookingData = new BookingDataDto();
				bookingData.setId(id);
				bookingData.setTargetCode(rs.getString("target_code"));
				bookingData.setStartDatetime(rs.getString("start_datetime"));
				bookingData.setEndDatetime(rs.getString("end_datetime"));
				bookingData.setUserName(rs.getString("user_name"));
				bookingData.setTitle(rs.getString("title"));
				bookingData.setDescription(rs.getString("description"));
				bookingData.setUpdateDatetime(rs.getString("update_datetime"));
			}

		} catch (SQLException e) {
			// SQL実行時の例外処理
			e.printStackTrace();
			throw new DataAccessException(e,
					"[BookingDataDao][selectData] Data select failed.");

		} finally {
			// 使い終わったオブジェクトは全て閉じておきましょう！
			this.close(rs);
			this.close(ps);
			this.close(con);
		}
		return bookingData;
	}

	/**
	 * 予約対象、検索期間開始日・終了日から<br>
	 * その期間内の予約情報をListに入れて返却します。<br>
	 *
	 * @param targetCode String 予約対象コード
	 * @param startDatetime String 検索期間開始日
	 * @param endDatetime String 検索期間終了日
	 * @return {@link List}<{@link BookingDataDto}>
	 * @throws DataAccessException "Data select failed."
	 */
	public List<BookingDataDto> selectDataList(String targetCode,
			String startDatetime, String endDatetime) throws DataAccessException {
		List<BookingDataDto> dataList = new ArrayList<BookingDataDto>();
		Connection con = getConnection();

		try {
			// privateな同名メソッドによりデータを取得
			dataList = this.selectDataList(con, targetCode, startDatetime, endDatetime);

		} catch (SQLException e) {
			// SQL実行時の例外処理
			e.printStackTrace();
			throw new DataAccessException(e,
					"[BookingDataDao][selectDataList] Data select failed.");

		} finally {
			// 使い終わったオブジェクトは全て閉じておきましょう！
			this.close(con);
		}
		return dataList;
	}

	/**
	 * 予約情報の新規登録もしくは更新を行います。<br>
	 * 予約期間内に他の予約が存在する場合は登録に失敗します。<br>
	 * 他の予約が存在しない場合には、<br>
	 * 予約IDが0を超過していればその予約情報を更新、<br>
	 * 予約IDが0（以下）ならば新規登録します。<br>
	 *
	 * @param data {@link BookingDataDto} クライアントから受け取った予約情報
	 * @return boolean 新規登録・内容更新成功以外はfalse
	 * @throws DataAccessException "Data select/insert failed."
	 */
	public boolean createOrUpdateData(BookingDataDto data) throws DataAccessException {
		boolean succeeded = false;
		Connection con = getConnection();
		String sqlType = "select";

		try {
			// 予約重複チェック
			// 予約情報のIDが同一な１件のみならば変更対象なので無視、
			// それ以外で１件以上あるならば重複
			List<BookingDataDto> dataList = this.selectDataList(con,
					data.getTargetCode(),
					data.getStartDatetime(),
					data.getEndDatetime());
			if ((dataList.size() >= 2) || (dataList.size() == 1
					&& dataList.get(0).getId() != data.getId())) {
				succeeded = false;
				this.setLatestMessage("他の予約と利用日時が重複しています。");

			} else {

				if (data.getId() > 0) {
					// 予約IDが0を超過していればその予約情報を更新
					sqlType = "update";
					if (this.updateData(con, data) > 0) {
						succeeded = true;
						this.setLatestMessage("予約内容を変更しました。");
					} else {
						this.setLatestMessage(
								"予約内容の変更に失敗しました。\r\n再処理してください");
					}

				} else {
					// 予約IDが0（以下）ならば新規登録
					sqlType = "insert";
					if (this.insertData(con, data) > 0) {
						succeeded = true;
						this.setLatestMessage("予約が完了しました。");
					} else {
						this.setLatestMessage(
								"予約に失敗しました。\r\n再処理してください");
					}
				}
			}

		} catch (SQLException e) {
			// SQL実行時の例外処理
			e.printStackTrace();
			throw new DataAccessException(e,
					"[BookingDataDao][addData] Data " + sqlType + " failed.");

		} finally {
			// 使い終わったオブジェクトは全て閉じておきましょう！
			this.close(con);
		}
		return succeeded;
	}

	/**
	 * IDに対応する予約情報を１件論理削除します。<br>
	 *
	 * @param id int 削除対象予約の予約ID
	 * @throws DataAccessException "Data update failed."
	 */
	public void deleteData(int id) throws DataAccessException {
	}

	/**
	 * 指定の予約対象について、期間内に存在するすべての予約データを取得します。 <br>
	 * <br>
	 * 期間開始日時～期間終了日時の範囲に含まれる、未削除の予約データを検索します。<br>
	 * ソート順は予約開始日時と予約終了日時の昇順です。<br>
	 * 範囲の検索条件は<b>下記①と⑤以外</b>となります。<br>
	 * <br>
	 * 　①【Ｘ】予約開始日時と予約終了日時が、いずれも期間開始日時以前<br>
	 * 　②【Ｏ】予約開始日時が期間開始日時以前で、かつ予約終了日時は期間内<br>
	 * 　③【Ｏ】予約開始日時が期間内で、かつ予約終了日時も期間内<br>
	 * 　④【Ｏ】予約開始日時が期間内で、かつ予約終了日時は期間終了日時以降<br>
	 * 　⑤【Ｘ】予約開始日時と予約終了日時が、いずれも期間終了日時以降<br>
	 * 　⑥【Ｏ】予約開始日時が期間開始日時以前で、かつ予約終了日時は期間終了日時以降<br>
	 * <br>
	 * 期間　　　　ＯＯＯＯＯＯＯＯ<br>
	 * ①　ＸＸＸＸ<br>
	 * ②　　　ＸＸＯＯ<br>
	 * ③　　　　　　　ＯＯＯＯ<br>
	 * ④　　　　　　　　　　　ＯＯＸＸ<br>
	 * ⑤　　　　　　　　　　　　　ＸＸＸＸ<br>
	 * ⑥　ＸＸＸＸＯＯＯＯＯＯＯＯＸＸＸＸ
	 * <br>
	 * この②③④⑥を抽出する方法は難しいため、逆に①⑤を除外する方法を考えます。まずは式にしてみます。<br>
	 * <br>
	 * ①を除外：<b>予約開始日時 <= 期間開始日時 AND 予約終了日時 <= 期間開始日時</b><br>
	 * ⑤を除外：<b>予約開始日時 >= 期間終了日時 AND 予約終了日時 >= 期間終了日時</b><br>
	 * <br>
	 * ここで、予約開始日時 < 予約終了日時は自明のため、それぞれ以下となります。<br>
	 * <br>
	 * ①を除外：<b>予約終了日時 <= 期間開始日時</b><br>
	 * ⑤を除外：<b>予約開始日時 >= 期間終了日時</b><br>
	 * <br>
	 * この２つを除外するため、式は次の通りとなります。<br>
	 * <br>
	 * <b>NOT ((予約終了日時 <= 期間開始日時) OR (予約開始日時 >= 期間終了日時))</b><br>
	 * <br>
	 * さらにここで「ド・モルガンの法則」により式を反転させる（NOTを取る）と・・・<br>
	 * <br>
	 * <b>(予約終了日時 > 期間開始日時) AND (予約開始日時 < 期間終了日時)</b><br>
	 * <br>
	 * となり、無事「期間内に存在するすべての予約データ」を抽出する条件となります。<br>
	 *
	 * @param con {@link Connection}
	 * @param targetCode String 予約対象コード
	 * @param startDatetime String 検索期間開始日
	 * @param endDatetime String 検索期間終了日
	 * @return {@link List}<{@link BookingDataDto}>
	 * @throws SQLException
	 */
	private List<BookingDataDto> selectDataList(Connection con,
			String targetCode, String startDatetime, String endDatetime) throws SQLException {
		List<BookingDataDto> dataList = new ArrayList<BookingDataDto>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sql = new StringBuilder("SELECT * FROM booking_data")
				.append(" WHERE target_code = ?")
				.append(" AND ((end_datetime > ?) AND (start_datetime < ?))")
				.append(" ORDER BY start_datetime , end_datetime;");

		try {
			// 用意したSQLに予約対象コードと検索期間を埋め込み（バインド）、実行
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, targetCode);
			ps.setString(2, startDatetime);
			ps.setString(3, endDatetime);
			rs = ps.executeQuery();

			// リサルトセットを１行ずつ回して、DTOに値を詰める
			while (rs.next()) {
				BookingDataDto data = new BookingDataDto();
				data.setId(rs.getInt("id"));
				data.setTargetCode(rs.getString("target_code"));
				data.setStartDatetime(rs.getString("start_datetime"));
				data.setEndDatetime(rs.getString("end_datetime"));
				data.setUserName(rs.getString("user_name"));
				data.setTitle(rs.getString("title"));
				data.setUpdateDatetime(rs.getString("update_datetime"));
				dataList.add(data);
			}

		} finally {
			// 使い終わったオブジェクトは全て閉じておきましょう！
			this.close(rs);
			this.close(ps);
		}
		return dataList;
	}

	/**
	 * 予約情報を更新します。<br>
	 *
	 * @param con {@link Connection}
	 * @param data {@link BookingDataDto} クライアントから受け取った予約情報
	 * @return int 更新件数
	 * @throws SQLException
	 */
	private int updateData(Connection con, BookingDataDto data) throws SQLException {
		int updatedCount = 0;
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder("UPDATE booking_data")
				.append(" SET target_code = ?, start_datetime = ?, end_datetime = ?")
				.append(", user_name = ?, title = ?, description = ?")
				.append(", update_datetime = ").append(this.getNowString())
				.append(" WHERE id = ?;");

		try {
			// 用意したSQLにDTOの値を埋め込み（バインド）、実行
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, data.getTargetCode());
			ps.setString(2, data.getStartDatetime());
			ps.setString(3, data.getEndDatetime());
			ps.setString(4, data.getUserName());
			ps.setString(5, data.getTitle());
			ps.setString(6, data.getDescription());
			ps.setInt(7, data.getId());
			updatedCount = ps.executeUpdate();
			this.commit(con);

		} finally {
			// 使い終わったオブジェクトは全て閉じておきましょう！
			this.close(ps);
		}
		return updatedCount;
	}

	/**
	 * 予約情報を登録します。<br>
	 *
	 * @param con {@link Connection}
	 * @param data {@link BookingDataDto} クライアントから受け取った予約情報
	 * @return int 登録件数
	 * @throws SQLException
	 */
	private int insertData(Connection con, BookingDataDto data) throws SQLException {
		int insertedCount = 0;
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder("INSERT INTO booking_data")
				.append(" (target_code, start_datetime, end_datetime")
				.append(", user_name, title, description, update_datetime)")
				.append(" VALUES (?, ?, ?, ?, ?, ?")
				.append(", ").append(this.getNowString()).append(");");

		try {
			// 用意したSQLにDTOの値を埋め込み（バインド）、実行
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, data.getTargetCode());
			ps.setString(2, data.getStartDatetime());
			ps.setString(3, data.getEndDatetime());
			ps.setString(4, data.getUserName());
			ps.setString(5, data.getTitle());
			ps.setString(6, data.getDescription());
			insertedCount = ps.executeUpdate();
			this.commit(con);

		} finally {
			// 使い終わったオブジェクトは全て閉じておきましょう！
			this.close(ps);
		}
		return insertedCount;
	}
}
