package jp.co.digskill.booking.model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import jp.co.digskill.booking.exception.DataAccessException;
import jp.co.digskill.booking.model.dto.BookingTargetDto;

/**
 * 予約対象データテーブル操作クラスです。<br>
 */
public class BookingTargetDao extends AbstractDao {

	/**
	 * コンストラクタ<br>
	 * データソースとそのRDBMSの種類を受け取り、<br>
	 * スーパークラスのコンストラクタを呼びます。<br>
	 *
	 * @param ds {@link DataSource} context.xmlから生成したデータソース
	 * @param dbType String web.xmlから取得したRDBMSの種類
	 */
	public BookingTargetDao(DataSource ds, String dbType) {
		super(ds, dbType);
	}

	/**
	 * 予約対象をすべて取得してListに入れて返却します。<br>
	 *
	 * @return {@link List}<{@link BookingTargetDto}>
	 * @throws DataAccessException "Data select failed."
	 */
	public List<BookingTargetDto> selectAllTarget() throws DataAccessException {
		List<BookingTargetDto> list = new ArrayList<BookingTargetDto>();
		Connection con = getConnection();
		Statement st = null;
		ResultSet rs = null;
		String sql = "SELECT * FROM booking_target ORDER BY id;";

		try {
			// SQLを実行
			st = con.createStatement();
			rs = st.executeQuery(sql);

			// リサルトセットを１行ずつ回して、DTOに値を詰める
			while (rs.next()) {
				BookingTargetDto target = new BookingTargetDto(
						rs.getInt("id"),
						rs.getString("code"),
						rs.getString("name"),
						rs.getString("information"));
				list.add(target);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			// SQL実行時の例外処理
			throw new DataAccessException(e,
					"[BookingTargetDao][selectAllTarget] Data select failed.");

		} finally {
			// 使い終わったオブジェクトは全て閉じておきましょう！
			this.close(rs);
			this.close(st);
			this.close(con);
		}
		return list;
	}
}
