package jp.co.digskill.booking.model.dao;

import java.lang.reflect.Constructor;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import jp.co.digskill.booking.exception.DataAccessException;

/**
 * DAO生成を管理するクラスです。<br>
 * DataSourceはweb.xmlとcontext.xmlに定義している<br>
 * DB設定情報から生成します。<br>
 */
public class DaoFactory {

	/**
	 * web.xmlとcontext.xmlに定義しているDB設定情報のキー
	 */
	private static final String DATA_SOURCE_INITIAL_CONTEXT_ID_PREFIX = "java:comp/env/jdbc/booking/";

	/**
	 * RDBMSのタイプとDAOクラス名からDAOをインスタンス化して返却します。<br>
	 *
	 * @param <T> {@link AbstractDao}を継承したDAOクラス
	 * @param dbType String web.xmlから取得したRDBMSの種類
	 * @param clazz {@link Class} インスタンス化したいDAOクラス
	 * @return <T>のインスタンス
	 * @throws DataAccessException "Create dao failed"
	 */
	@SuppressWarnings("unchecked")
	public static <T extends AbstractDao> T createDao(String dbType, Class<T> clazz) throws DataAccessException {
		try {
			Constructor<?> constructor = clazz.getConstructor(DataSource.class, String.class);
			return (T) constructor.newInstance(DaoFactory.getDataSource(dbType), dbType);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessException(e,
					"[DaoFactory][createDao] Create dao failed.");
		}
	}

	/**
	 * web.xmlから取得したRDBMSの種類を基にcontext.xmlから<br>
	 * DataSourceを生成して返却します。<br>
	 *
	 * @param dbType String web.xmlから取得したRDBMSの種類
	 * @return {@link DataSource} context.xmlから生成したデータソース
	 * @throws DataAccessException "DataSource missing"
	 */
	private static DataSource getDataSource(String dbType) throws DataAccessException {

		DataSource ds = null;
		try {
			InitialContext context = new InitialContext();
			ds = (DataSource) context.lookup(DATA_SOURCE_INITIAL_CONTEXT_ID_PREFIX + dbType);
			context.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new DataAccessException(e,
					"[DaoFactory][getDataSource] DataSource missing.");
		}

		return ds;
	}
}
