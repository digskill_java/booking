package jp.co.digskill.booking.model.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import jp.co.digskill.booking.exception.DataAccessException;

/**
 * データアクセス実行クラスの抽象クラスです。<br>
 * RDBMSの違いによる差異を吸収しますが、基本的には<br>
 * DB接続オブジェクトを子クラスへ渡して直接操作させます。<br>
 */
public abstract class AbstractDao {

	/**
	 * 接続するDBに応じた処理を行うための分岐キーワード<br>
	 */
	private static final String DB_TYPE_MYSQL = "mysql";
	private static final String DB_TYPE_SQLITE = "sqlite";

	/**
	 * 現在時刻のタイムスタンプをSQLに埋め込むための文字列<br>
	 */
	private static final String NOW_STRING_MYSQL = "DATE_FORMAT(LOCALTIME, '%Y/%m/%d %H:%i:%S')";
	private static final String NOW_STRING_SQLITE = "strftime('%Y/%m/%d %H:%M:%S', CURRENT_TIMESTAMP, 'localtime')";

	/**
	 * web.xmlとcontext.xmlから取得・生成した情報をプロパティとして保持<br>
	 */
	protected DataSource ds;
	protected String dbType;

	/**
	 * コンストラクタ<br>
	 * データソースとそのRDBMSの種類を受け取り、<br>
	 * 子クラスからのDB接続要求に備えます。<br>
	 *
	 * @param ds {@link DataSource} context.xmlから生成したデータソース
	 * @param dbType String web.xmlから取得したRDBMSの種類
	 */
	protected AbstractDao(DataSource ds, String dbType) {
		this.ds = ds;
		this.dbType = dbType;
	}

	/**
	 * RDBMSの種類に応じた、現在時刻のタイムスタンプを<br>
	 * SQLに埋め込むための文字列を返します。<br>
	 *
	 * @return String 現在時刻タイムスタンプ生成文字列
	 */
	protected String getNowString() {
		return this.dbType == AbstractDao.DB_TYPE_MYSQL ? AbstractDao.NOW_STRING_MYSQL
				: this.dbType == AbstractDao.DB_TYPE_SQLITE ? AbstractDao.NOW_STRING_SQLITE : "";
	}

	/**
	 * DB接続オブジェクトを返します。<br>
	 *
	 * @return {@link Connection} DB接続オブジェクト
	 * @throws DataAccessException "Connection failed."
	 */
	protected Connection getConnection() throws DataAccessException {
		Connection con;
		try {
			con = this.ds.getConnection();
			con.setAutoCommit(false);
		} catch (SQLException e) {
			throw new DataAccessException(e,
					"[AbstractDao][getConnection] Connection failed.");
		}
		return con;
	}

	/**
	 * 実行中のトランザクションを確定させます。<br>
	 *
	 * @param con {@link Connection} DB接続オブジェクト
	 * @throws DataAccessException "Commit failed."
	 */
	protected void commit(Connection con) throws DataAccessException {
		if (con != null) {
			try {
				con.commit();
			} catch (SQLException e) {
				throw new DataAccessException(e,
						"[AbstractDao][commit] Commit failed.");
			}
		}
	}

	/**
	 * 実行中のトランザクションを破棄し、巻き戻します。<br>
	 *
	 * @param con {@link Connection} DB接続オブジェクト
	 * @throws DataAccessException "Rollback failed."
	 */
	protected void rollback(Connection con) throws DataAccessException {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				throw new DataAccessException(e,
						"[AbstractDao][rollback] Rollback failed.");
			}
		}
	}

	/**
	 * 接続中のDB接続オブジェクトを閉じます。<br>
	 *
	 * @param con {@link Connection} DB接続オブジェクト
	 * @throws DataAccessException "Connection already closed.");."
	 */
	protected void close(Connection con) throws DataAccessException {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				throw new DataAccessException(e,
						"[AbstractDao][close(con)] Connection already closed.");
			}
		}
	}

	/**
	 * 使用中のステートメントを閉じます。<br>
	 *
	 * @param st {@link Statement} ステートメント
	 * @throws DataAccessException "Statement already closed.");."
	 */
	protected void close(Statement st) throws DataAccessException {
		if (st != null) {
			try {
				st.close();
			} catch (SQLException e) {
				throw new DataAccessException(e,
						"[AbstractDao][close(ps)] Statement already closed.");
			}
		}
	}

	/**
	 * 使用中のリサルトセットを閉じます。<br>
	 *
	 * @param rs {@link ResultSet} リサルトセット
	 * @throws DataAccessException "ResultSet already closed.");."
	 */
	protected void close(ResultSet rs) throws DataAccessException {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				throw new DataAccessException(e,
						"[AbstractDao][close(rs)] ResultSet already closed.");
			}
		}
	}
}
