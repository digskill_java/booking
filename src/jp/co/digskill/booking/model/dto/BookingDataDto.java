package jp.co.digskill.booking.model.dto;

import java.io.Serializable;

/**
 * 予約情報オブジェクト<br>
 */
public class BookingDataDto implements Serializable {

	public static final String TABLE = "BOOKING_DATA";

	private int id;
	private String targetCode;
	private String startDatetime;
	private String endDatetime;
	private String userName;
	private String title;
	private String description;
	private String updateDatetime;

	/**
	 * 引数ありコンストラクタ
	 *
	 * @param id int
	 */
	public BookingDataDto(int id) {
		this.id = id;
	}

	/**
	 * コンストラクタ
	 */
	public BookingDataDto() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTargetCode() {
		return this.targetCode;
	}

	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}

	public String getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(String startDatetime) {
		this.startDatetime = startDatetime;
	}

	public String getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(String endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUpdateDatetime() {
		return this.updateDatetime;
	}

	public void setUpdateDatetime(String updateDatetime) {
		this.updateDatetime = updateDatetime;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[BookingDataDto:");
		buffer.append(" id: ");
		buffer.append(this.id);
		buffer.append(" targetCode: ");
		buffer.append(this.targetCode);
		buffer.append(" startDatetime: ");
		buffer.append(this.startDatetime);
		buffer.append(" endDatetime: ");
		buffer.append(this.endDatetime);
		buffer.append(" userName: ");
		buffer.append(this.userName);
		buffer.append(" title: ");
		buffer.append(this.title);
		buffer.append(" description: ");
		buffer.append(this.description);
		buffer.append(" updateDatetime: ");
		buffer.append(this.updateDatetime);
		buffer.append("]");
		return buffer.toString();
	}

}
