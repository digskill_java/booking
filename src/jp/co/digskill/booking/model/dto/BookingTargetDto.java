package jp.co.digskill.booking.model.dto;

import java.io.Serializable;

/**
 * 予約対象情報オブジェクト<br>
 */
public class BookingTargetDto implements Serializable {

	public static final String TABLE = "BOOKING_TARGET";

	private int id;
	private String code;
	private String name;
	private String information;

	/**
	 * 引数ありコンストラクタ
	 *
	 * @param id int
	 * @param code String
	 * @param name String
	 * @param information String
	 */
	public BookingTargetDto(int id, String code, String name, String information) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.information = information;
	}

	/**
	 * コンストラクタ
	 */
	public BookingTargetDto() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInformation() {
		return this.information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[BookingTargetDto:");
		buffer.append(" id: ");
		buffer.append(this.id);
		buffer.append(" code: ");
		buffer.append(this.code);
		buffer.append(" name: ");
		buffer.append(this.name);
		buffer.append(" information: ");
		buffer.append(this.information);
		buffer.append("]");
		return buffer.toString();
	}

}
