package jp.co.digskill.booking.exception;

/**
 * DBアクセスを伴うデータ処理における実行時例外オブジェクト
 */
public final class DataAccessException extends RuntimeException {

	/**
	 * 任意のエラーメッセージと元の実行時例外のメッセージを基に<br>
	 * 新たな実行時例外を生成します。<br>
	 * また、同時に標準出力へのダンプも行います。<br>
	 *
	 * @param e {@link Exception} 元の実行時例外オブジェクト
	 * @param message String 任意のエラーメッセージ
	 */
	public DataAccessException(Exception e, String message) {
		super(message + "<br />" + e.getMessage());
		e.printStackTrace();
	}

	/**
	 * 任意のエラーメッセージと詳細情報、<br>
	 * および元の実行時例外のメッセージを基に<br>
	 * 新たな実行時例外を生成します。<br>
	 * また、同時に標準出力へのダンプも行います。<br>
	 *
	 * @param e {@link Exception} 元の実行時例外オブジェクト
	 * @param information String 任意の詳細情報
	 * @param message String 任意のエラーメッセージ
	 */
	public DataAccessException(Exception e, String information, String message) {
		super(message + "<br />" + information + "<br />" + e.getMessage());
		e.printStackTrace();
	}

	/**
	 * 任意のエラーメッセージを基に新たな実行時例外を生成します。<br>
	 *
	 * @param message String 任意のエラーメッセージ
	 */
	public DataAccessException(String message) {
		super(message);
	}
}
