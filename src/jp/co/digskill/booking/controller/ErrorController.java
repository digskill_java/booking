package jp.co.digskill.booking.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 実行時例外発生時にエラー画面を表示するためのクラスです。<br>
 * マッピングすべきURLは存在しないため、先頭のアノテーションでは<br>
 * {@code name} 及び {@code urlPatterns} を定義していません。
 */
@WebServlet()
public class ErrorController extends AbstractController {

	/**
	 * 渡されたエラーオブジェクトを基にエラー画面を表示します。<br>
	 *
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @param e {@link Throwable} 発生したエラーオブジェクト
	 * @param containerWidth int web.xmlの設定情報
	 * @throws IOException
	 * @throws ServletException
	 */
	public void doResponse(HttpServletRequest req, HttpServletResponse res,
			Throwable e) throws IOException, ServletException {

		// JSPへエラーメッセージを渡す
		req.setAttribute("errorMessage", e.getMessage());

		// 設定情報を取得
		this.getConfig(req.getServletContext());

		// 結果をHTMLで返す
		this.doHtmlResponse(req, res, "error.jsp");
	}
}