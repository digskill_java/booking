package jp.co.digskill.booking.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import jp.co.digskill.booking.exception.DataAccessException;
import jp.co.digskill.booking.model.dao.BookingDataDao;
import jp.co.digskill.booking.model.dao.BookingTargetDao;
import jp.co.digskill.booking.model.dao.DaoFactory;
import jp.co.digskill.booking.model.dto.BookingDataDto;
import jp.co.digskill.booking.model.dto.BookingTargetDto;

/**
 * このアプリケーションのエントリーポイントとしてのControllerです。<br>
 * パラメーター「a」の値によりActionに振り分けて処理を実行します。<br>
 * 先頭のアノテーションにより「http:// ～ /Booking/index/」への処理を受け付けます。
 */
@WebServlet(name = "IndexController", urlPatterns = { "/index/" })
public class IndexController extends AbstractController {

	/**
	 * GETメソッドによるアクセスをdoResponseへ渡します。<br>
	 *
	 * @see {@link IndexController#doResponse}
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		doResponse(req, res);
	}

	/**
	 * POSTメソッドによるアクセスをdoResponseへ渡します。<br>
	 *
	 * @see {@link IndexController#doResponse}
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @throws IOException
	 * @throws ServletException
	 */
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {
		doResponse(req, res);
	}

	/**
	 * パラメーター「a」の値によりActionに振り分けて処理を実行します。<br>
	 * エラー発生時は{@link ErrorController#doResponse}へ処理を渡します。<br>
	 *
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @throws IOException
	 * @throws ServletException
	 */
	public void doResponse(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {

		// 受け取るパラメータの文字コード指定
		req.setCharacterEncoding("UTF-8");

		try {
			// パラメータ「a」で処理を切り分ける
			String action = req.getParameter("a");
			if (action == null || action.isEmpty() || "index".equals(action)) {
				// 一覧画面の表示
				this.indexAction(req, res);
			} else if ("input".equals(action)) {
				// 予約登録
				this.inputAction(req, res);
			} else if ("get".equals(action)) {
				// 予約ダイアログ情報取得
				this.getAction(req, res);
			} else if ("cancel".equals(action)) {
				// 予約キャンセル
				this.cancelAction(req, res);
			}

		} catch (Throwable e) {
			// 実行時エラー発生時の処理
			ErrorController err = new ErrorController();
			err.doResponse(req, res, e);
		}
	}

	/**
	 * 以下の処理を行って予約情報カレンダーを表示します。<br>
	 * <ul>
	 * <li>予約対象のリストをDBより取得
	 * <li>予約対象と表示年月パラメーターを取得・加工
	 * <li>表示年月内の予約データをDBより取得
	 * </ul>
	 *
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @throws IOException
	 * @throws ServletException
	 * @see /WebContent/WEB-INF/jsp/error.jsp
	 */
	private void indexAction(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException {

		// 予約対象リストをDBから取得する
		BookingTargetDao targetDao = DaoFactory.<BookingTargetDao> createDao(this.dbType, BookingTargetDao.class);
		List<BookingTargetDto> targetList = targetDao.selectAllTarget();

		// 予約対象のコードをパラメータ「tc」から取得する。
		// 予約対象リストに該当するコードが無い場合は
		// 予約対象リストの先頭データからコードを設定する。
		String targetCode = req.getParameter("tc");
		BookingTargetDto bookingTarget = null;
		for (BookingTargetDto target : targetList) {
			if (target.getCode().equals(targetCode)) {
				bookingTarget = target;
			}
		}
		if (bookingTarget == null) {
			bookingTarget = targetList.get(0);
		}
		targetCode = bookingTarget.getCode();

		// yyyymm形式の表示年月をパラメータ「tm」から取得する。
		// 有効な年月で無い場合は今日の年月を設定する。
		// 正規表現「^\\d{6}$」は「先頭から末尾の間に数字6桁のみ」
		String targetMonth = req.getParameter("tm");
		boolean tmIsAvailable = false;
		if (targetMonth != null && targetMonth.matches("^\\d{6}$")) {
			int targetYyyy = Integer.parseInt(targetMonth.substring(0, 4));
			int targetMm = Integer.parseInt(targetMonth.substring(4, 6));
			if ((targetYyyy >= 1) && (targetYyyy <= 9999)
					&& (targetMm >= 1) && (targetMm <= 12)) {
				tmIsAvailable = true;
			}
		}
		SimpleDateFormat yyyymm = new SimpleDateFormat("yyyyMM");
		Calendar now = Calendar.getInstance();
		if (!tmIsAvailable) {
			targetMonth = yyyymm.format(now.getTime());
		}

		// 表示するカレンダー＆予約データの範囲　＝　表示年月「１日の00:00」から
		Calendar startDate = Calendar.getInstance();
		startDate.set(Calendar.YEAR, Integer.parseInt(targetMonth.substring(0, 4)));
		startDate.set(Calendar.MONTH, Integer.parseInt(targetMonth.substring(4, 6)) - 1);
		startDate.set(Calendar.DATE, 1);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);

		// 表示するカレンダー＆予約データの範囲　＝　表示年月「末日の23:59」まで
		int lastDayOfMonth = startDate.getActualMaximum(Calendar.DAY_OF_MONTH);
		Calendar endDate = (Calendar) startDate.clone();
		endDate.set(Calendar.DATE, lastDayOfMonth);
		endDate.set(Calendar.HOUR_OF_DAY, 23);
		endDate.set(Calendar.MINUTE, 59);

		// DBから予約データ取得
		BookingDataDao dataDao = DaoFactory.<BookingDataDao> createDao(this.dbType, BookingDataDao.class);
		SimpleDateFormat dbDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		List<BookingDataDto> bookingList = dataDao.selectDataList(targetCode,
				dbDateFormat.format(startDate.getTime()), dbDateFormat.format(endDate.getTime()));

		// JSPへ渡すもの
		req.setAttribute("targetMonth", targetMonth);
		req.setAttribute("startDate", startDate);
		req.setAttribute("endDate", endDate);
		req.setAttribute("bookingTarget", bookingTarget);
		req.setAttribute("targetList", targetList);
		req.setAttribute("bookingList", bookingList);

		// 結果をHTMLで返す
		this.doHtmlResponse(req, res, "index.jsp");
	}

	/**
	 * 以下の処理を行って予約情報の新規登録か予約内容変更を行います。<br>
	 * クライアントからは非同期通信によるリクエストとなり、<br>
	 * レスポンスはJSON形式で処理の成否とメッセージを返却します。<br>
	 * <ul>
	 * <li>リクエストパラメーターを詰めたDTOを生成
	 * <li>DAOへDB書き込み処理を指示
	 * <li>JSON形式でレスポンスを返却
	 * </ul>
	 *
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @throws JSONException
	 * @throws IOException
	 */
	private void inputAction(HttpServletRequest req, HttpServletResponse res)
			throws JSONException, IOException {

		boolean succeeded = false;
		String message = "";

		// 予約データをdaoへ渡すための入れ物（dto）を生成する。
		BookingDataDto data = new BookingDataDto();

		// リクエストパラメータから入力内容をdtoへセットする。
		// 数値項目はjavascript側で「-0」により数値化している。
		// 「id」が「0」ならば新規予約、それ以外は予約内容変更。
		// 本来はパラメータが正しいかというチェックが必要。
		data.setId(Integer.parseInt(req.getParameter("id")));
		data.setTargetCode(req.getParameter("tc"));
		data.setUpdateDatetime(req.getParameter("update_datetime"));
		data.setStartDatetime(req.getParameter("start_datetime"));
		data.setEndDatetime(req.getParameter("end_datetime"));
		data.setTitle(req.getParameter("title"));
		data.setUserName(req.getParameter("user_name"));
		data.setDescription(req.getParameter("description"));

		// DB書込開始
		BookingDataDao dao = DaoFactory.<BookingDataDao> createDao(this.dbType, BookingDataDao.class);
		try {
			succeeded = dao.createOrUpdateData(data);
			message = dao.getLatestMessage();

		} catch (DataAccessException e) {
			message = e.getMessage();
		}

		// 結果をJSONで返す
		this.doJsonResponse(res, succeeded, message, null);
	}

	/**
	 * 以下の処理を行って予約情報詳細の取得を行います。<br>
	 * クライアントからは非同期通信によるリクエストとなり、<br>
	 * レスポンスはJSON形式で処理の成否とメッセージ、<br>
	 * 成功時は予約情報詳細データを返却します。<br>
	 * <ul>
	 * <li>リクエストパラメーターのidチェック
	 * <li>DAOへDB検索処理を指示
	 * <li>JSON形式でレスポンスを返却
	 * </ul>
	 *
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @throws JSONException
	 * @throws IOException
	 */
	private void getAction(HttpServletRequest req, HttpServletResponse res)
			throws JSONException, IOException {

		boolean succeeded = false;
		String message = "";
		JSONObject data = null;

		// 取得する予約データの「id」をパラメータから取得する。
		// パラメータが無い、もしくは数値でなければエラーを返す。
		String paramId = req.getParameter("id");
		if (paramId == null || paramId.isEmpty() || !paramId.matches("^\\d+$")) {
			succeeded = false;
			message = "パラメータ取得エラー：id";

		} else {

			// 予約データを取得する
			BookingDataDao dao = DaoFactory.<BookingDataDao> createDao(this.dbType, BookingDataDao.class);
			BookingDataDto bookingData = null;
			try {
				bookingData = dao.selectData(Integer.parseInt(paramId));

			} catch (DataAccessException e) {
				message = e.getMessage();
			}

			if (bookingData == null) {
				message = "対象の予約は既にキャンセルされています。";

			} else {
				succeeded = true;
				// 予約データをJSON化する
				data = new JSONObject(bookingData);
			}
		}

		// 結果をJSONで返す
		this.doJsonResponse(res, succeeded, message, data);
	}

	/**
	 * 以下の処理を行って予約情報の取消を行います。<br>
	 * クライアントからは非同期通信によるリクエストとなり、<br>
	 * レスポンスはJSON形式で処理の成否とメッセージを返却します。<br>
	 * <ul>
	 * <li>リクエストパラメーターのidチェック
	 * <li>DAOへDB更新（論理削除）処理を指示
	 * <li>JSON形式でレスポンスを返却
	 * </ul>
	 *
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @throws JSONException
	 * @throws IOException
	 */
	private void cancelAction(HttpServletRequest req, HttpServletResponse res)
			throws JSONException, IOException {
	}
}