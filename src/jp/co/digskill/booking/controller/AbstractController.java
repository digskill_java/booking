package jp.co.digskill.booking.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 各Controllerクラスの抽象クラスです。<br>
 * HttpServletを継承しており、<br>
 * レスポンスの書き出しなどの共通処理を行います。
 */
public class AbstractController extends HttpServlet {

	/**
	 * web.xmlから設定情報を取得するためのキー<br>
	 */
	protected static final String CONFIG_DB_TYPE = "dbType";
	protected static final String CONFIG_BOOKING_RANGE_OFFSET_YEARS = "bookingRangeOffsetYears";
	protected static final String CONFIG_BOOKING_TIME_SLOT_START_HOUR = "bookingTimeSlotStartHour";
	protected static final String CONFIG_BOOKING_TIME_SLOT_END_HOUR = "bookingTimeSlotEndHour";
	protected static final String CONFIG_CONTAINER_WIDTH = "containerWidth";

	/**
	 * web.xmlから取得した設定情報を一時的に格納するための変数<br>
	 */
	protected String dbType = "";
	protected int bookingRangeOffsetYears = 0;
	protected int bookingTimeSlotStartHour = 0;
	protected int bookingTimeSlotEndHour = 0;
	protected int containerWidth = 0;

	/**
	 * 初期処理としてweb.xmlから設定情報を取得します。<br>
	 *
	 * @throws ServletException ServletContextからの値取得に失敗した場合
	 */
	@Override
	public void init() throws ServletException {
		this.getConfig(getServletContext());
	}

	/**
	 * web.xmlからの設定情報を取得します。<br>
	 * forward先からも実行可能とするため、<br>
	 * ServletContextは引数で受け取る方式としています。<br>
	 *
	 * @param context {@link ServletContext}
	 */
	protected void getConfig(ServletContext context) {
		// DBはどれを使うか
		this.dbType = context.getInitParameter(AbstractController.CONFIG_DB_TYPE);
		// カレンダー画面を表示できる範囲（1ならば前後12か月選択可能）
		this.bookingRangeOffsetYears = Integer.parseInt(context.getInitParameter(
				AbstractController.CONFIG_BOOKING_RANGE_OFFSET_YEARS));
		// 表示する時間帯の開始
		this.bookingTimeSlotStartHour = Integer.parseInt(context.getInitParameter(
				AbstractController.CONFIG_BOOKING_TIME_SLOT_START_HOUR));
		// 表示する時間帯の終了
		this.bookingTimeSlotEndHour = Integer.parseInt(context.getInitParameter(
				AbstractController.CONFIG_BOOKING_TIME_SLOT_END_HOUR));
		// カレンダー表示領域の幅
		this.containerWidth = Integer.parseInt(context.getInitParameter(
				AbstractController.CONFIG_CONTAINER_WIDTH));
	}

	/**
	 * JSPへHTML出力処理を指示する際の共通処理を行います。<br>
	 *
	 * @param req {@link HttpServletRequest}
	 * @param res {@link HttpServletResponse}
	 * @param jspPath String forword先JSPへのjsp格納フォルダからの相対パス
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void doHtmlResponse(HttpServletRequest req, HttpServletResponse res, String jspPath)
			throws IOException, ServletException {

		// 送り出すHTMLの文字コード指定
		res.setContentType("text/html; charset=UTF-8");

		// web.xmlのconfig情報を設定
		req.setAttribute(AbstractController.CONFIG_BOOKING_RANGE_OFFSET_YEARS, this.bookingRangeOffsetYears);
		req.setAttribute(AbstractController.CONFIG_BOOKING_TIME_SLOT_START_HOUR, this.bookingTimeSlotStartHour);
		req.setAttribute(AbstractController.CONFIG_BOOKING_TIME_SLOT_END_HOUR, this.bookingTimeSlotEndHour);
		req.setAttribute(AbstractController.CONFIG_CONTAINER_WIDTH, this.containerWidth);

		// JSPへ処理を移す
		RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/jsp/" + jspPath);
		rd.forward(req, res);
	}

	/**
	 * JSONによりレスポンスを返却する際の共通処理を行います。<br>
	 *
	 * @param res {@link HttpServletResponse}
	 * @param succeeded boolean JSONへ含ませる処理結果成否
	 * @param message String JSONへ含ませる処理結果メッセージ
	 * @param data JSONObject JSONへ含ませる処理結果データ
	 * @throws JSONException
	 * @throws IOException
	 */
	protected void doJsonResponse(HttpServletResponse res,
			boolean succeeded, String message, JSONObject data)
			throws JSONException, IOException {

		// 結果をJSONで返す。
		JSONObject json = new JSONObject();
		json.put("succeeded", succeeded);
		json.put("message", message);
		json.put("data", data);

		// 送り出すJSONの文字コード指定
		res.setContentType("text/javascript; Charset=UTF-8");
		res.getWriter().write(json.toString());
	}

}
